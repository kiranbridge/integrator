import React from "react";
import "../Product/Product.css";
import { Container, Row, Col } from "reactstrap";
export default class Product extends React.Component {
  render() {
    return (
      <div className="product-block">
        <Row className="same-height">
          <Col md="6" className="align-items-center">
            <div className="pb-left">
              <h2>Prices</h2>
              <p className="small-text">
                Get an all new certification or renew your current.
              </p>
              <div className="small-line" />
              <img src={require("../img/horn.png")} alt="horn" />
              <p className="smallest-text">
                Tell the word about your Integrator Certification
              </p>
            </div>
          </Col>
          <Col md="6" className="pb-right">
            <Row className="row">
              <Col className="product-item">
                <h1 className="price text-center">€950</h1>
                <p className="smallest-text text-center">
                  FULL DAY CERTIFIED INTEGRATOR
                </p>
                <ul>
                  <li>
                    <a>Full day lessons</a>
                  </li>
                  <li>
                    <a>Case Study</a>
                  </li>
                  <li>
                    <a>Examination</a>
                  </li>
                  <li>
                    <a>Two-year certificate</a>
                  </li>
                </ul>
                <button className="int-btn blue-btn d-block mx-auto">
                  <img
                    src={require("../img/lightning.png")}
                    alt="lightning"
                    className="img-fluid"
                  />{" "}
                  PURCHASE
                </button>
              </Col>
              <Col className="product-item">
                <h1 className="price text-center">€450</h1>
                <p className="smallest-text text-center">CERTIFICATION ONLY</p>
                <ul>
                  <li>
                    <a>Case Study</a>
                  </li>
                  <li>
                    <a>Examination</a>
                  </li>
                  <li>
                    <a>Two-year certificate</a>
                  </li>
                </ul>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}
