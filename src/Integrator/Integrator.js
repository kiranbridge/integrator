import React from "react";
import "../Integrator/Integrator.css";
import {
  Container,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Col
} from "reactstrap";
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody
} from "react-accessible-accordion";
import "react-accessible-accordion/dist/fancy-example.css";

import classnames from "classnames";

export default class Integrator extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: "1"
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    return (
      <Container className="content-wrapper container p-t-0">
        <div className="testimonial-block d-xl-block d-lg-block d-md-block d-sm-none d-none">
          <TabContent activeTab={this.state.activeTab}>
            <img
              className="testimonial-block-img img-fluid d-block mx-auto"
              src={require("../img/testimoial-img.png")}
              alt="testimoial"
            />
            <TabPane tabId="1">
              <Col
                sm="12"
                md={{ size: 5, offset: 7 }}
                className="testimonial-block-content"
              >
                <h1>
                  1This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </Col>
            </TabPane>
            <TabPane tabId="2">
              <Col
                sm="12"
                md={{ size: 5, offset: 7 }}
                className="testimonial-block-content"
              >
                <h1>
                  2This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </Col>
            </TabPane>
            <TabPane tabId="3">
              <Col
                sm="12"
                md={{ size: 5, offset: 7 }}
                className="testimonial-block-content"
              >
                <h1>
                  3This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </Col>
            </TabPane>
            <TabPane tabId="4">
              <Col
                sm="12"
                md={{ size: 5, offset: 7 }}
                className="testimonial-block-content"
              >
                <h1>
                  4This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </Col>
            </TabPane>
            <TabPane tabId="5">
              <Col
                sm="12"
                md={{ size: 5, offset: 7 }}
                className="testimonial-block-content"
              >
                <h1>
                  5This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </Col>
            </TabPane>
          </TabContent>
          <nav className="testimonial-nav-block">
            <Nav tabs>
              <NavItem className="col">
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "1"
                  })}
                  onClick={() => {
                    this.toggle("1");
                  }}
                >
                  <div className="arrow-down" />
                  <img
                    src={require("../img/1.png")}
                    alt="user1"
                    className="img-fluid d-block mx-auto"
                  />
                  <p className="tstml-name">David Rasky</p>
                  <p className="tstml-pos">Manager</p>
                </NavLink>
              </NavItem>
              <NavItem className="col">
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "2"
                  })}
                  onClick={() => {
                    this.toggle("2");
                  }}
                >
                  <div className="arrow-down" />
                  <img
                    src={require("../img/2.png")}
                    alt="user2"
                    className="img-fluid d-block mx-auto"
                  />
                  <p className="tstml-name">David Rasky</p>
                  <p className="tstml-pos">Manager</p>
                </NavLink>
              </NavItem>
              <NavItem className="col">
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "3"
                  })}
                  onClick={() => {
                    this.toggle("3");
                  }}
                >
                  <div className="arrow-down" />
                  <img
                    src={require("../img/2.png")}
                    alt="user2"
                    className="img-fluid d-block mx-auto"
                  />
                  <p className="tstml-name">David Rasky</p>
                  <p className="tstml-pos">Manager</p>
                </NavLink>
              </NavItem>
              <NavItem className="col">
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "4"
                  })}
                  onClick={() => {
                    this.toggle("4");
                  }}
                >
                  <div className="arrow-down" />
                  <img
                    src={require("../img/2.png")}
                    alt="user2"
                    className="img-fluid d-block mx-auto"
                  />
                  <p className="tstml-name">David Rasky</p>
                  <p className="tstml-pos">Manager</p>
                </NavLink>
              </NavItem>
              <NavItem className="col">
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "5"
                  })}
                  onClick={() => {
                    this.toggle("5");
                  }}
                >
                  <div className="arrow-down" />
                  <img
                    src={require("../img/2.png")}
                    alt="user2"
                    className="img-fluid d-block mx-auto"
                  />
                  <p className="tstml-name">David Rasky</p>
                  <p className="tstml-pos">Manager</p>
                </NavLink>
              </NavItem>
            </Nav>
          </nav>
        </div>
        <div className="testimonial-block d-sm-block d-md-none d-lg-none d-xl-none">
          <Accordion>
            <AccordionItem>
              <AccordionItemTitle>
                <img src={require("../img/1.png")} alt="user1" className="" />
                <p className="tstml-name">David Rasky</p>
                <p className="tstml-pos">Manager</p>
              </AccordionItemTitle>
              <AccordionItemBody>
                <h1>
                  This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </AccordionItemBody>
            </AccordionItem>
            <AccordionItem>
              <AccordionItemTitle>
                <img src={require("../img/1.png")} alt="user1" className="" />
                <p className="tstml-name">David Rasky</p>
                <p className="tstml-pos">Manager</p>
              </AccordionItemTitle>
              <AccordionItemBody>
                <h1>
                  This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </AccordionItemBody>
            </AccordionItem>
            <AccordionItem>
              <AccordionItemTitle>
                <img src={require("../img/1.png")} alt="user1" className="" />
                <p className="tstml-name">David Rasky</p>
                <p className="tstml-pos">Manager</p>
              </AccordionItemTitle>
              <AccordionItemBody>
                <h1>
                  This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </AccordionItemBody>
            </AccordionItem>
            <AccordionItem>
              <AccordionItemTitle>
                <img src={require("../img/1.png")} alt="user1" className="" />
                <p className="tstml-name">David Rasky</p>
                <p className="tstml-pos">Manager</p>
              </AccordionItemTitle>
              <AccordionItemBody>
                <h1>
                  This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </AccordionItemBody>
            </AccordionItem>
            <AccordionItem>
              <AccordionItemTitle>
                <img src={require("../img/1.png")} alt="user1" className="" />
                <p className="tstml-name">David Rasky</p>
                <p className="tstml-pos">Manager</p>
              </AccordionItemTitle>
              <AccordionItemBody>
                <h1>
                  This certificate ensure our teams live up to our standards.
                </h1>
                <p className="tstml-name">Peter Blomqvist</p>
                <p className="tstml-pos">
                  Application Integration Manager at Volvo Cars
                </p>
                <img
                  src={require("../img/volvo-logo-scaled.png")}
                  alt="Volvo"
                  className="img-fluid tstml-logo"
                />
              </AccordionItemBody>
            </AccordionItem>
          </Accordion>
        </div>
      </Container>
    );
  }
}
