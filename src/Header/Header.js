import React from "react";
import logo from "../img/integrator_logo.png";
import "../Header/Header.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faAlignJustify } from "@fortawesome/free-solid-svg-icons";

import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
library.add(faAlignJustify);
export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <Container>
        <Navbar light expand="md">
          <NavbarBrand href="/">
            <img src={logo} className="img-fluid logo" alt="Integrator" />
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink href="/">About Certified Integrator </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/Certificate">Get certified</NavLink>
              </NavItem>
            </Nav>
            {/* <span>
              <FontAwesomeIcon className="oi" icon="align-justify" />
            </span> */}
          </Collapse>
        </Navbar>
      </Container>
    );
  }
}
