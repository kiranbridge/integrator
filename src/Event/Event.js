import React from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import events from "../Event/events";
import "../Event/Event.css";
// import Register from "../Event/Register";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import Registerme from "../Event/Registerme";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faCheck,
  faChevronLeft,
  faChevronRight,
  faClock
} from "@fortawesome/free-solid-svg-icons";
library.add(faTimes, faCheck, faChevronLeft, faChevronRight, faClock);
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer
function Events({ event }) {
  return (
    <div id="selectme">
      <span id={event.title}>
        <strong>{event.title}</strong>
        {event.desc && ":  " + event.desc}
      </span>
      <Registerme />
    </div>
  );
}
const CustomToolbar = toolbar => {
  const goToBack = () => {
    toolbar.date.setMonth(toolbar.date.getMonth() - 1);
    toolbar.onNavigate("prev");
  };

  const goToNext = () => {
    toolbar.date.setMonth(toolbar.date.getMonth() + 1);
    toolbar.onNavigate("next");
  };

  const goToCurrent = () => {
    const now = new Date();
    toolbar.date.setMonth(now.getMonth());
    toolbar.date.setYear(now.getFullYear());
    toolbar.onNavigate("current");
  };

  const label = () => {
    const date = moment(toolbar.date);
    return (
      <span className="tb-month">
        {date.format("MMMM")}
        <span className="tb-Year"> / {date.format("YYYY")}</span>
      </span>
    );
  };

  return (
    <div className="toolbar-container">
      <label className="label-date">{label()}</label>

      <div className="back-next-buttons">
        <button className="btn-back btn-calendar" onClick={goToBack}>
          <FontAwesomeIcon className="oi" icon="chevron-left" />
        </button>
        {/* <button className="btn-current btn-calendar" onClick={goToCurrent}>
          today
        </button> */}
        <button className="btn-next btn-calendar" onClick={goToNext}>
          <FontAwesomeIcon className="oi" icon="chevron-right" />
        </button>
      </div>
    </div>
  );
};

BigCalendar.momentLocalizer(moment);
export default class Event extends React.Component {
  render() {
    return (
      <Container className="calendar-block">
        <h3 className="text-center">Find a certification session</h3>
        <Row>
          <BigCalendar
            className="col-md-12"
            events={events}
            timeslots={4}
            components={{
              toolbar: CustomToolbar,
              event: Events
            }}
            // onSelectEvent={this.props.onSelectEvent}
            className="calender-wrapper"
            // onSelectEvent={this.toggle}
            views={["month"]}
            eventPropGetter={this.eventStyleGetter}
          />
        </Row>
      </Container>
    );
  }
}
