import React from "react";
import Header from "../Header/Header";
import Body from "../Body/Body";
import Getcertificate from "../Getcertificate/Getcertificate";
import Footer from "../Footer/Footer";

export default class Certificate extends React.Component {
  render() {
    return (
      <div className="integrator-wrapper">
        <Header />
        <Body />
        <Getcertificate />
        <Footer />
      </div>
    );
  }
}
