import React from "react";
import "../Body/Body.css";
import { Container, Row, Col } from "reactstrap";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCheck } from "@fortawesome/free-solid-svg-icons";
library.add(faTimes, faCheck);
export default class Body extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const currentPath = window.location.pathname;

    return (
      <Container className="content-wrapper p-b-0">
        <Row className="top-banner">
          <Col md="6" className="left">
            <div>
              <p className="small-text">
                {currentPath === "/"
                  ? "GET CONTROL OVER DISTRIBUTED INTEGRATION"
                  : "GET AHEAD OF THE COMPETITION"}
              </p>
              <h1>
                {currentPath === "/"
                  ? "Require Certified Integrators"
                  : "Prove your skills, get certified"}
              </h1>
              <ul>
                <li>
                  <button className="int-btn blue-btn">Learn More</button>
                </li>
                <li>
                  <button className="int-btn green-btn">
                    {currentPath === "/"
                      ? "get certified"
                      : "REGISTER FOR A CERTIFICATION"}
                  </button>
                </li>
              </ul>
            </div>
          </Col>
          <Col md="6" className="right">
            <img
              className="img-fluid"
              src={require("../img/integrator-process.png")}
              alt="Integrator"
            />
          </Col>
        </Row>
        <div className="under-banner">
          <h2 className="text-center">
            {currentPath === "/"
              ? "Don’t end up with spaghetti and headache. Make sure your vendors are meeting the standards of todays integrations. Look for the Certified Integrator badge."
              : "Stay up-to-date on the latest in end-to-end integration and integration landscapes. Join the certified professionals building integrations faster and smarter."}
          </h2>
          <div className="logo-b">
            <img
              className="img-fluid mx-auto d-block"
              src={require("../img/integrator_logo_small.png")}
              alt="Integrator logo"
            />
          </div>
          <h3 className="text-center">Why use Certified Integrators?</h3>
        </div>
        <div className="row cretificate-box-block">
          <Col md="6" className="cretificate-box-left card">
            <Col className="card-body col">
              <h5 className="card-title">WITH CERTIFIED INTEGRATORS</h5>
              <ul>
                <li>
                  <FontAwesomeIcon className="oi" icon="check" />
                  Get in control of distributed integration with standards to
                  ensure quality
                </li>
                <li>
                  <FontAwesomeIcon className="oi" icon="check" />
                  Confirmed knowledge of the 10 vitalities for an end-to-end
                  integration
                </li>
                <li>
                  <FontAwesomeIcon className="oi" icon="check" />
                  Knowledge about the five vitalities for a landscape of
                  integrations
                </li>
                <li>
                  <FontAwesomeIcon className="oi" icon="check" />
                  Skill & experience assessed in certification examination,
                  including case study
                </li>
              </ul>
            </Col>
          </Col>
          <Col md="6" className="cretificate-box-right card">
            <Col className="card-body col">
              <h5 className="card-title">WITH NON-CERTIFIED INTEGRATORS</h5>
              <ul>
                <li>
                  <FontAwesomeIcon className="oi" icon="times" />
                  Process results from a change in the energy level of the
                  nucleus to a lower state, resulting
                </li>
                <li>
                  <FontAwesomeIcon className="oi" icon="times" />
                  Results from a change in the energy level of the nucleus to a
                  lower state, resulting in the
                </li>
                <li>
                  <FontAwesomeIcon className="oi" icon="times" />
                  Other more rare types of radioactive decay include ejection of
                  neutrons or protons or
                </li>
                <li>
                  <FontAwesomeIcon className="oi" icon="times" />
                  Each radioactive isotope has a characteristic decay time
                  period—the half-life—that is
                </li>
              </ul>
            </Col>
          </Col>
        </div>
      </Container>
    );
  }
}
