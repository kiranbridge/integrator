import React from "react";
import { Container, Row, Col } from "reactstrap";
import Hubspot from "../Hubspot/Hubspot";
export default class Getcertificate extends React.Component {
  render() {
    return (
      <Container>
        <div className="how-work-block d-md-block">
          <h3 className="text-center">How does it work?</h3>
          <div className="row">
            <Col md="4" className="hwb-box">
              <img
                src={require("../img/logo-icon.png")}
                alt="integrator"
                className="img-fluid hwb-img"
              />
              <h4>Certification sessions</h4>
              <p className="small-text">
                Certified Integrator is available as a full day learning session
                together with the certification exam and as certification exam
                only (for renewal).
                <br />
                <a href="#">See session agenda</a>
              </p>
            </Col>
            <Col md="4" className="hwb-box">
              <img
                src={require("../img/mic.png")}
                alt="integrator"
                className="img-fluid hwb-img"
              />
              <h4>Online & Onsite</h4>
              <p className="small-text">
                Certification sessions are available on a regular basis, both
                online and in different countries and cities. Check the calendar
                below for a session.
                <br />
                <a href="#">Find a session</a>
              </p>
            </Col>
            <Col md="4" className="hwb-box">
              <img
                src={require("../img/clock.png")}
                alt="integrator"
                className="img-fluid hwb-img"
              />
              <h4>Valid for two-years</h4>
              <p className="small-text">
                The Certified Integrator are valid for two years. We’ll contact
                you when it is time for renewal.
              </p>
            </Col>
          </div>
        </div>
        <Hubspot />
        <div className="faq-block">
          <h3 className="text-center">Frequently Asked Questions</h3>
          <div className="row faq-items">
            <div className="col-sm faq-item">
              <ul>
                <li className="question">
                  <span>Q</span>
                  <p> Why should I get certified?</p>
                </li>
                <li className="answer">
                  <span>A</span>
                  <p>
                    {" "}
                    Stay up-to-date on the best practices ways for end-to-end
                    integration landscapes.{" "}
                  </p>
                </li>
              </ul>
            </div>
            <div className="col-sm faq-item">
              <ul>
                <li className="question">
                  <span>Q</span>
                  <p> Which devices are supported?</p>
                </li>
                <li className="answer">
                  <span>A</span>
                  <p>
                    {" "}
                    Stability of isotopes is affected by the ratio of protons to
                    neutrons, also by the presence of certain.{" "}
                  </p>
                </li>
              </ul>
            </div>
          </div>
          <div className="row faq-items">
            <div className="col-sm faq-item">
              <ul>
                <li className="question">
                  <span>Q</span>
                  <p> Why should I get certified?</p>
                </li>
                <li className="answer">
                  <span>A</span>
                  <p>
                    {" "}
                    Stay up-to-date on the best practices ways for end-to-end
                    integration landscapes.{" "}
                  </p>
                </li>
              </ul>
            </div>
            <div className="col-sm faq-item">
              <ul>
                <li className="question">
                  <span>Q</span>
                  <p> Which devices are supported?</p>
                </li>
                <li className="answer">
                  <span>A</span>
                  <p>
                    {" "}
                    Stability of isotopes is affected by the ratio of protons to
                    neutrons, also by the presence of certain.{" "}
                  </p>
                </li>
              </ul>
            </div>
          </div>
          <div className="row faq-items">
            <div className="col-sm faq-item">
              <ul>
                <li className="question">
                  <span>Q</span>
                  <p> Why should I get certified?</p>
                </li>
                <li className="answer">
                  <span>A</span>
                  <p>
                    {" "}
                    Stay up-to-date on the best practices ways for end-to-end
                    integration landscapes.{" "}
                  </p>
                </li>
              </ul>
            </div>
            <div className="col-sm faq-item">
              <ul>
                <li className="question">
                  <span>Q</span>
                  <p> className devices are supported?</p>
                </li>
                <li className="answer">
                  <span>A</span>
                  <p>
                    {" "}
                    Stability of isotopes is affected by the ratio of protons to
                    neutrons, also by the presence of certain.{" "}
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </Container>
    );
  }
}
