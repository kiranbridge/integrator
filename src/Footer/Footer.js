import React from "react";
import "../Footer/Footer.css";
import { Row, Col, Container, Card, CardBody } from "reactstrap";

export default class Example extends React.Component {
  render() {
    const currentPath = window.location.pathname;
    return (
      <footer>
        <Container>
          <Card className="col-md-10 offset-md-1 crt-box-block">
            <CardBody className="row">
              <Col md="6">
                <div className="small-text">
                  {currentPath === "/" ? "DEVELOPER?" : null}
                </div>
                <p>
                  {currentPath === "/"
                    ? "Get ahead of the competition, join the certified professionals building integrations faster and smarter."
                    : "Can't find answer? Don’t worry, our support team will help you"}
                </p>
              </Col>
              <Col
                md="6"
                className="align-self-center justify-content-center d-flex"
              >
                <button className="int-btn green-btn">GET CERTIFIED</button>
              </Col>
            </CardBody>
          </Card>
        </Container>
        <div className="footer-wrapper">
          <Row>
            <Col md="6">
              <img
                className="img-fluid logo"
                alt="logo-integrators"
                src={require("../img/integrator_logo.png")}
              />
              <p>
                Stay up-to-date on the latest in end-to-end integration and
                integration landscapes. Get ahead of the competition, join the
                certified professionals building integrations faster and smarter
                for a better digital world.
              </p>
            </Col>
            <Col md="6" className="text-right align-self-center">
              <ul>
                <li>
                  <a href="/">ABOUT CERTIFIED INTEGRATOR</a>
                </li>
                <li>
                  <a href="/Certificate">GET CERTIFIED</a>
                </li>
              </ul>
            </Col>
          </Row>
        </div>
      </footer>
    );
  }
}
