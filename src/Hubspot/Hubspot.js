import React from "react";
import "../Hubspot/Hubspot.css";
import $ from "jquery";
import { Tooltip } from "reactstrap";

export default class Hubspot extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false
    };
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }
  componentDidMount() {
    const script = document.createElement("script");
    script.src = "https://js.hsforms.net/forms/v2.js";
    document.body.appendChild(script);

    script.addEventListener("load", () => {
      if (window.hbspt) {
        window.hbspt.forms.create({
          portalId: "2277117",
          formId: "34c921dc-7c58-4339-b77a-de985925d7bd",
          target: "#hubspotForm"
        });
      }
    });
    setTimeout(function() {
      // $(".hs-form-booleancheckbox label span span").append("<h1>thing.4</h1>");
      var large =
        '<span class="tooltip1">Terms and Conditions<span class="tooltiptext1">Entiros is committed to protecting and respecting your privacy, and we’ll only use your personal information to administer your account and to provide the products and services you requested from us. From time to time, we would like to contact you about our products and services, as well as other content that may be of interest to you. If you consent to us contacting you for this purpose, please tick below to say how you would like us to contact you:<br>You may unsubscribe from these communications at any time. For more information on how to unsubscribe, our privacy practices, and how we are committed to protecting and respecting your privacy, please review our Privacy Policy. By clicking submit below, you consent to allow Entiros to store and process the personal information submitted above to provide you the content requested.</span></span>';

      $(large).insertAfter(".hs-form-booleancheckbox label span span");
    }, 4000);
  }

  render() {
    return (
      <div className="hubspot-wrapper">
        <div className="hubspot-container">
          <h3 className="text-center">Get certified</h3>
          <p>
            Fill out the form and we will contact you with the available
            certification dates
          </p>
          <div id="hubspotForm" className="d-block mx-auto" />
        </div>
      </div>
    );
  }
}
