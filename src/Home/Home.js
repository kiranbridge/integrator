import React from "react";
import Header from "../Header/Header";
import Body from "../Body/Body";
import Integrator from "../Integrator/Integrator";
import Footer from "../Footer/Footer";

export default class Home extends React.Component {
  render() {
    return (
      <div className="integrator-wrapper">
        <Header />
        <Body />
        <Integrator />
        <Footer />
      </div>
    );
  }
}
