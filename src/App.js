import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./Home/Home";
import Certificate from "./Certificate/Certificate";
import "./Responsive/Responsive.css";
class App extends Component {
  render() {
    return (
      <Router>
        <div className="integrator-wrapper">
          <Route exact path="/" component={Home} />
          <Route exact path="/Certificate" component={Certificate} />
        </div>
      </Router>
    );
  }
}

export default App;
